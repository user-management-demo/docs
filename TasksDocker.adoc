= Tasks Docker

* create docker-compose file
** mysql database version 8.0.21
*** set maxPacketSize 15mb
*** port 3309
*** root password: root
*** create user: usermanageradmin password: usermanageradmin
*** database: users
*** add script to create users and run script on startup (backend: src/main/resources/data.sql)
** external auth service
*** create dockerfile based on centos/systemd
*** install java
*** set java_home
*** add user/group:tomcat password:tomcat
*** start jar via java -jar as tomcat user
** netzwerk: cg_se_docker_network


== Goal

* database available jdbc:mysql://localhost:3309/users with user usermanageradmin and password usermanageradmin
* external auth available: http://localhost:8089/service/external-auth/info
* directory structure
** data/jarfile
** settings/mysql_custom.cnf
** docker-compose.yml
** dockerfile-extauth